/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainclassCharacter;
import java.util.*;
/**
 *
 * @author Konsta
 */
public class MainclassCharacter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean bool = true;
        Character newCharacter = new Character();
        System.out.println("*** TAISTELUSIMULAATTORI ***"
                + "\n1) Luo hahmo"
                + "\n2) Taistele hahmolla"
                + "\n0) Lopeta"
                + "\nValintasi:");
        Scanner input = new Scanner(System.in);
        int choice = input.nextInt();
        while(bool)
        switch(choice){
            case 1:
                System.out.println("Valitse hahmosi:"
                        + "\n1) Kuningas"
                        + "\n2) Ritari"
                        + "\n3) Kuningatar"
                        + "\n4) Peikko"
                        + "\nValintasi:");
                choice = input.nextInt();
                switch(choice){
                    case 1:
                        newCharacter.king.King();
                        System.out.println("Valitse aseesi:"
                                + "\n1) Veitsi"
                                + "\n2) Kirves"
                                + "\n3) Miekka"
                                + "\n4) Nuija"
                                + "\nValintasi");
                        choice = input.nextInt();
                        switch(choice){
                            case 1:
                                newCharacter.wepBehavior.knife.KnifeBehavior();
                                break;
                            case 2:
                                newCharacter.wepBehavior.club.ClubBehavior();
                                break;
                            case 3:
                                newCharacter.wepBehavior.axe.AxeBehavior();
                                break;
                            case 4:
                                newCharacter.wepBehavior.sword.SwordBehavior();
                                break;
                            default:
                                System.out.println("Väärä valinta");
                        }
                        break;
                    case 2:
                        newCharacter.knight.Knight();
                        System.out.println("Valitse aseesi:"
                                + "\n1) Veitsi"
                                + "\n2) Kirves"
                                + "\n3) Miekka"
                                + "\n4) Nuija"
                                + "\nValintasi");
                        choice = input.nextInt();
                        switch(choice){
                            case 1:
                                newCharacter.wepBehavior.knife.KnifeBehavior();
                                break;
                            case 2:
                                newCharacter.wepBehavior.club.ClubBehavior();
                                break;
                            case 3:
                                newCharacter.wepBehavior.axe.AxeBehavior();
                                break;
                            case 4:
                                newCharacter.wepBehavior.sword.SwordBehavior();
                                break;
                            default:
                                System.out.println("Väärä valinta");
                        }
                        break;
                    case 3:
                        newCharacter.queen.Queen();
                        System.out.println("Valitse aseesi:"
                                + "\n1) Veitsi"
                                + "\n2) Kirves"
                                + "\n3) Miekka"
                                + "\n4) Nuija"
                                + "\nValintasi");
                        choice = input.nextInt();
                        switch(choice){
                            case 1:
                                newCharacter.wepBehavior.knife.KnifeBehavior();
                                break;
                            case 2:
                                newCharacter.wepBehavior.club.ClubBehavior();
                                break;
                            case 3:
                                newCharacter.wepBehavior.axe.AxeBehavior();
                                break;
                            case 4:
                                newCharacter.wepBehavior.sword.SwordBehavior();
                                break;
                            default:
                                System.out.println("Väärä valinta");
                        }
                        break;
                    case 4:
                        newCharacter.troll.Troll();
                        System.out.println("Valitse aseesi:"
                                + "\n1) Veitsi"
                                + "\n2) Kirves"
                                + "\n3) Miekka"
                                + "\n4) Nuija"
                                + "\nValintasi");
                        choice = input.nextInt();
                        switch(choice){
                            case 1:
                                newCharacter.wepBehavior.knife.KnifeBehavior();
                                break;
                            case 2:
                                newCharacter.wepBehavior.club.ClubBehavior();
                                break;
                            case 3:
                                newCharacter.wepBehavior.axe.AxeBehavior();
                                break;
                            case 4:
                                newCharacter.wepBehavior.sword.SwordBehavior();
                                break;
                            default:
                                System.out.println("Väärä valinta");
                        }
                        break;
                    default:
                        System.out.println("Väärä valinta");
                }
                break;
            case 2:
                System.out.println(newCharacter.characterType + " tappelee aseella " + newCharacter.wepBehavior.weaponType);
                break;
            case 0:
                bool = false;
                break;
            default:
                System.out.println("Väärä valinta");
        }
    }
    
}
