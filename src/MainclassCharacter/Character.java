/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MainclassCharacter;

/**
 *
 * @author Konsta
 */
public class Character {
    public String characterType;
    King king = new King();
    Queen queen = new Queen();
    Knight knight = new Knight();
    Troll troll = new Troll();
    WeaponBehavior wepBehavior = new WeaponBehavior();
    
    public class King extends Character{
        public void King(){
            this.characterType = "King";
        }
    }
    public class Queen extends Character{
        public void Queen(){
            this.characterType = "Queen";
        }
    }
    public class Knight extends Character{
        public void Knight(){
            this.characterType = "Knight";
        }  
    }
    public class Troll extends Character{
        public void Troll(){
            this.characterType = "Troll";
        } 
    }
    
    public class WeaponBehavior{
        public String weaponType;
        KnifeBehavior knife = new KnifeBehavior();
        ClubBehavior club = new ClubBehavior();
        AxeBehavior axe = new AxeBehavior();
        SwordBehavior sword = new SwordBehavior();
        
        public class ClubBehavior extends WeaponBehavior{
            public void ClubBehavior(){
                this.weaponType = "Club";
            }
        }
        public class AxeBehavior extends WeaponBehavior{
            public void AxeBehavior(){
                this.weaponType = "Axe";
            }
        }
        public class SwordBehavior extends WeaponBehavior{
            public void SwordBehavior(){
                this.weaponType = "Sword";
            }
        }
        public class KnifeBehavior extends WeaponBehavior{
            public void KnifeBehavior(){
                this.weaponType = "Knife";
            }
        }
    }
}
