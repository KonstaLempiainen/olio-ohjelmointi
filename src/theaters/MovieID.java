/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package theaters;

/**
 *
 * @author Konsta
 */
public class MovieID {
    private String id;
    private String place;
    private String start;
    private String end;
    private String name;

    public String getName() {
        return name;
    }
    
    public String getId() {
        return id;
    }
    public String getPlace() {
        return place;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public MovieID(String place, String start, String end, String id, String name){
        this.place = place;
        this.start = start.substring(11, 16);
        this.end = end.substring(11, 16);
        this.id = id;
        this.name = name;
    }
    @Override
    public String toString(){
        return name + " Paikassa: " + place + " Alkaa: " + start + " Loppuu: " + end;
    }
}
