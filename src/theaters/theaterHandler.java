/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package theaters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Konsta
 */
public class theaterHandler {
    
    static private theaterHandler handler = null;
    private ArrayList<Theater> arrList;
    private Document doc;
    private HashMap<String, String> map;

    public HashMap<String, String> getMap() {
        return map;
    }
    
    public ArrayList<Theater> getArrList() {
        return arrList;
    }
    private theaterHandler(String text){
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(text)));
            doc.getDocumentElement().normalize();
            
            map = new HashMap();
            arrList = new ArrayList();
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(theaterHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    static public theaterHandler getInstance() throws IOException{
        String urlText = "http://www.finnkino.fi/xml/TheatreAreas/";
        String text = xmlHandler(urlText);
        if(handler == null){
            handler = new theaterHandler(text);
        }
        return handler;
    }
    private void addTheater(String name, String id){
        arrList.add(new Theater(name, id));
    }
    private static String xmlHandler(String text) throws MalformedURLException, IOException{
        URL url = new URL(text);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        String content = "";
        
        while((line = br.readLine())!= null){
            content += line + "\n";
        }
        return content;
    }
    
    private void parseCurrentData(){
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        for(int i = 1; i < nodes.getLength();i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            map.put(getValue("Name", e), getValue("ID", e));
            
            addTheater(getValue("Name", e),getValue("ID", e));
        }
    }
    private String getValue(String tag, Element e){
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
}
