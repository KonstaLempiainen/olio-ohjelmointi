/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package theaters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Konsta
 */
public class Movies {
static private Movies movies = null;
    private ArrayList<Movie> arrList;
    private ArrayList<MovieID> arrListID;
    private Document doc;
    private HashMap<String, String> map;
    
    public ArrayList<MovieID> getArrListID() {
        return arrListID;
    }
    public HashMap<String, String> getMap() {
        return map;
    }
    
    public ArrayList<Movie> getArrList() {
        return arrList;
    }
    private Movies(String text){
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(text)));
            doc.getDocumentElement().normalize();
            
            map = new HashMap();
            arrList = new ArrayList();
            arrListID = new ArrayList();
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(theaterHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    static public Movies getNewInstance(String text) throws IOException{
        movies = new Movies(text);
        return movies;
    }
    static public Movies getInstance(String text) throws IOException{
        if(movies == null){
            movies = new Movies(text);
        }
        return movies;
    }
    private void addMovie(String name, String start, String end){
        arrList.add(new Movie(name, start, end));
    }
    private void addMovieID(String place, String start, String end, String id, String name){
        arrListID.add(new MovieID(place, start, end, id, name));
    }
    
    private void parseCurrentData(){
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 0; i < nodes.getLength();i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            map.put(getValue("Title", e), getValue("EventID", e));
            
            addMovie(getValue("Title", e),getValue("dttmShowStart", e), getValue("dttmShowEnd", e));
            addMovieID(getValue("Theatre", e),getValue("dttmShowStart", e), getValue("dttmShowEnd", e),getValue("EventID", e), getValue("Title", e));
        }
    }
    private String getValue(String tag, Element e){
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
}
