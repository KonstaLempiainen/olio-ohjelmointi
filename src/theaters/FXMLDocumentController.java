/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package theaters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Taisto
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Label labelTheater;
    @FXML
    private Label titleLabel;
    @FXML
    private Label labelTime;
    @FXML
    private ComboBox<Theater> theaterMenu;
    @FXML
    private TextField startingTime;
    @FXML
    private TextField endingTime;
    @FXML
    private TextField dayInput;
    @FXML
    private Button listButton;
    @FXML
    private TextField movieName;
    @FXML
    private Button searchButton;
    @FXML
    private TextArea outputLabel;
    @FXML
    private ListView<Movie> listView;
    @FXML
    private ListView<MovieID> listView2;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        theaterHandler th;
        try {
            th = theaterHandler.getInstance();
            for(int i = 0; i<th.getArrList().size();i++){
                theaterMenu.getItems().add(th.getArrList().get(i));
            }
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void listByTheater(ActionEvent event) throws IOException, ParseException {
        theaterHandler th;
        th = theaterHandler.getInstance();
        String id = th.getMap().get(theaterMenu.getValue().getTheaterName());
        String date = dayInput.getText();
        if(date.isEmpty()){
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date dateObject = new Date();
            date = dateFormat.format(dateObject);
        }

        String urlText = "http://www.finnkino.fi/xml/Schedule/?area=" + id + "&dt=" + date;
        
        URL url = new URL(urlText);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        String content = "";
        
        while((line = br.readLine())!= null){
            content += line + "\n";
        }
        outputLabel.setText(content);
        listView.getItems().clear();
        Movies m = Movies.getNewInstance(content);
        if(startingTime.getText().isEmpty() && endingTime.getText().isEmpty()){
            for(int i = 0; i<m.getArrList().size();i++){
                listView.getItems().add(m.getArrList().get(i)); 
            }
        }
        else if(endingTime.getText().isEmpty()){
            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            Date sTime = parser.parse(startingTime.getText());
            Date movieStartTime;
            for(int i = 0; i<m.getArrList().size();i++){
                movieStartTime  = parser.parse(m.getArrList().get(i).getStart().substring(11, 16));
                if(movieStartTime.after(sTime)){
                    listView.getItems().add(m.getArrList().get(i)); 
                }
            }
        }
        else if(startingTime.getText().isEmpty()){
            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            Date eTime = parser.parse(endingTime.getText());
            Date movieEndTime;
            for(int i = 0; i<m.getArrList().size();i++){
                movieEndTime  = parser.parse(m.getArrList().get(i).getEnd().substring(11, 16));
                if(movieEndTime.before(eTime)){
                    listView.getItems().add(m.getArrList().get(i)); 
                }
            }
        }
        else{
            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            Date eTime = parser.parse(endingTime.getText());
            Date sTime = parser.parse(startingTime.getText());
            Date movieStartTime;
            Date movieEndTime;
            for(int i = 0; i<m.getArrList().size();i++){
                movieStartTime  = parser.parse(m.getArrList().get(i).getStart().substring(11, 16));
                movieEndTime  = parser.parse(m.getArrList().get(0).getEnd().substring(11, 16));
                if(movieEndTime.before(eTime) && movieStartTime.after(sTime)){
                    listView.getItems().add(m.getArrList().get(i)); 
                }
            }
        }
    }


    @FXML
    private void searchByName(ActionEvent event) throws IOException {
        URL url1 = new URL("http://www.finnkino.fi/xml/Schedule");
        BufferedReader br1 = new BufferedReader(new InputStreamReader(url1.openStream()));
        String line1;
        String content1 = "";
        
        while((line1 = br1.readLine())!= null){
            content1 += line1 + "\n";
        }
        Movies m1 = Movies.getNewInstance(content1);
        System.out.println(m1.getMap().get(movieName.getText()));
        
        URL url = new URL("http://www.finnkino.fi/xml/Schedule/?eventID=" + m1.getMap().get(movieName.getText()));
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String line;
        String content = "";
        
        while((line = br.readLine())!= null){
            content += line + "\n";
        }
        Movies m = Movies.getNewInstance(content);
        outputLabel.setText(content);
        listView2.getItems().clear();
        for(int i = 0; i<m.getArrList().size();i++){
            listView2.getItems().add(m.getArrListID().get(i)); 
        }
    }

}
