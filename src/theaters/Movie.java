/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package theaters;

/**
 *
 * @author Konsta
 */
public class Movie {
    private String name;
    private String start;
    private String end;

    public String getName() {
        return name;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public Movie(String name, String start, String end){
        this.name = name;
        this.start = start;
        this.end = end;
    }
    @Override
    public String toString(){
        return name;
    }
}
