/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainclassFight;

import java.util.Scanner;

/**
 *
 * @author Taisto
 */
public class MainclassFight {
    
    public static void main(String[] args) {
        
    boolean bool = true;
    int choice;
    
        while(bool){
            choice = printMenu();
            switch(choice){
                case 1:
                    chooseChar();
                    break;
                case 2:
                    System.out.println(newChar.characterType + " taistelee aseella " + newChar.weaponType);
                    break;
                case 0:
                    bool = false;
                    break;
                default:
                    System.out.println("Väärä valinta");
            }
        }
    }
    
    static public void chooseWep(){
        Character newChar = new Character();
        System.out.println("Valitse aseesi:"
                                + "\n1) Veitsi"
                                + "\n2) Kirves"
                                + "\n3) Miekka"
                                + "\n4) Nuija"
                                + "\nValintasi");
        int choice;
        try (Scanner input = new Scanner(System.in)) {
            choice = input.nextInt();
        }
        switch(choice){
            case 1:
                newChar.wepBehavior.knife.KnifeBehavior();
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
        }
    }
    static public void chooseChar(){
        Character newChar = new Character();
        System.out.println("Valitse hahmosi:"
                        + "\n1) Kuningas"
                        + "\n2) Ritari"
                        + "\n3) Kuningatar"
                        + "\n4) Peikko"
                        + "\nValintasi:");
        int choice;
        try (Scanner input = new Scanner(System.in)) {
            choice = input.nextInt();
        }
        switch(choice){
            case 1:
                newChar.king.King();
                chooseWep();
                break;
            case 2:
                chooseWep();
                break;
            case 3:
                chooseWep();
                break;
            case 4:
                chooseWep();
                break;
        }
    }
    static public int printMenu(){
        System.out.println("*** TAISTELUSIMULAATTORI ***"
                            + "\n1) Luo hahmo"
                            + "\n2) Taistele hahmolla"
                            + "\n0) Lopeta"
                            + "\nValintasi:");
        int choice;
        try (Scanner input = new Scanner(System.in)) {
            choice = input.nextInt();
        }
        return choice;
    }
}
