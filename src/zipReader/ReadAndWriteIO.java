/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package zipReader;

import java.io.*;
import java.util.zip.*;
/**
 *
 * @author Konsta
 */
public class ReadAndWriteIO {

    public void readAndWrite(String fileName){
        try{
            ZipFile zFile = new ZipFile(fileName);
            InputStream input = zFile.getInputStream(new ZipEntry("input.txt"));
            BufferedReader br = new BufferedReader(new InputStreamReader(input, "UTF-8"));
            String inputLine;
            while((inputLine = br.readLine()) != null){
		System.out.println(inputLine);
            }     
        }catch(IOException i){
            i.printStackTrace();
        }
    }
}
