/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mainBank2;

import java.util.Scanner;
import java.util.ArrayList;


/**
 *
 * @author Konsta
 */
public class Bank {
    ArrayList<Account> arrayList = new ArrayList();
    

    public Account search(String accountNumber){
        int i = 0;
        boolean match = false;
        Account returnValue = null;
        while(i < arrayList.size() && match == false){
            if(arrayList.get(i).accountNumber.equals(accountNumber)){
                match = true;
                returnValue =  arrayList.get(i);
                i++;
            }
            else{
                returnValue = null;
                i++;
            }
        }
        return returnValue;
    }

    public void addAccount(){
        Scanner input = new Scanner(System.in);
        String accountNumber;
        String amountTxT;
        Account result;
        
        System.out.print("Syötä tilinumero: ");
        accountNumber = input.nextLine();
        System.out.print("Syötä rahamäärä: ");
        amountTxT = input.nextLine();
        Double amount = Double.parseDouble(amountTxT);
        result = search(accountNumber);
        if(result != null){
            System.out.println("Tili on jo olemassa");
        }
        else{
            arrayList.add(new CurrentAccount(amount, accountNumber));
            System.out.println("Tili luotu.");
        }
    }
    public void addCreditAccount(){
        Scanner input = new Scanner(System.in);
        String accountNumber;
        String amountTxT;
        String creditTxT;
        Account result;
        
        System.out.print("Syötä tilinumero: ");
        accountNumber = input.nextLine();
        System.out.print("Syötä rahamäärä: ");
        amountTxT = input.nextLine();
        Double amount = Double.parseDouble(amountTxT);
        System.out.print("Syötä luottoraja: ");
        creditTxT = input.nextLine();
        Double credit = Double.parseDouble(creditTxT);
        result = search(accountNumber);
        if(result != null){
            System.out.println("Tili on jo olemassa");
        }
        else{
            arrayList.add(new CreditAccount(amount, accountNumber, credit));
            System.out.println("Tili luotu.");
        }
    }
    public void deleteAccount(){
        Scanner input = new Scanner(System.in);
        String accountNumber;
        Account result;
        System.out.print("Syötä poistettava tilinumero: ");
        accountNumber = input.nextLine();
        result = search(accountNumber);
        if(result == null){
            System.out.println("Tiliä ei löydy");
        }
        else{
            System.out.println("Tili poistettu.");
            arrayList.remove(arrayList.indexOf(result));
        }
    }
    public void printAccount(){
        Account result;
        Scanner input = new Scanner(System.in);
        String accountNumber;
        System.out.print("Syötä tulostettava tilinumero: ");
        accountNumber = input.nextLine();
        result = search(accountNumber);
        if(result == null){
            System.out.println("Tiliä ei löydy");
        }
        else if(result.getAccountType()){
            System.out.println("Tilinumero: " + result.getAccountNumber());
            System.out.println("Rahamäärä: " + Math.round(result.getBalance()));
            System.out.println("Luottoraja: " + Math.round(result.getCreditLimit()));
        }
        else{
            System.out.print("Tilinumero: " + result.getAccountNumber());
            System.out.println("Tilillä rahaa: " + Math.round(result.getBalance()));
        }
    }
    public void printAll(){
        System.out.println("Kaikki tilit:");
        /*Account result;
        for(int i = 0; i < arrayList.size(); i++){
            result = arrayList.get(i);
            if(result.getAccountType()){
                System.out.println("Tilinumero: " + result.getAccountNumber());
                System.out.println("Rahamäärä: " + Math.round(result.getBalance()));
                System.out.println("Luottoraja: " + Math.round(result.getCreditLimit()));
            }
            else{
                System.out.print("Tilinumero: " + result.getAccountNumber());
                System.out.println("Tilillä rahaa: " + Math.round(result.getBalance()));
            }
        }*/
    }
    public void addMoney(){
        Account result;
        Scanner input = new Scanner(System.in);
        String accountNumber;
        String amountTxT;
        System.out.print("Syötä tilinumero: ");
        accountNumber = input.nextLine();
        result = search(accountNumber);
        System.out.print("Syötä rahamäärä: ");
        amountTxT = input.nextLine();
        Double amount = Double.parseDouble(amountTxT);
        if(result == null){
            System.out.println("Tiliä ei löydy");
        }
        else{
            result.setBalance(amount);
            System.out.println("Talletetaan tilille: " + result.getAccountNumber() + " rahaa " + amount);
        }
    }
    public void takeMoney(){
        Account result;
        Scanner input = new Scanner(System.in);
        String accountNumber;
        String amountTxT;
        System.out.print("Syötä tilinumero: ");
        accountNumber = input.nextLine();
        result = search(accountNumber);
        System.out.print("Syötä rahamäärä: ");
        amountTxT = input.nextLine();
        Double amount = Double.parseDouble(amountTxT);
        if(result == null){
            System.out.println("Tiliä ei löydy");
        }
        else{
            if(result.getAccountType()){
                if((result.getBalance()+result.getCreditLimit()) > 0){
                    result.setBalance(-amount);
                }
                else{
                    System.out.println("Tilillä ei ole rahaa.");
                }
            }
            else{
                if(result.getBalance() > 0){
                    result.setBalance(-amount);
                }
                else{
                    System.out.println("Tilillä ei ole rahaa.");    
                }
            }
        }
    }
}
