/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainBank2;
import java.util.Scanner;
/**
 *
 * @author Konsta
 */
public class MainclassBank2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean bool = true;
        Bank bank = new Bank();
        Scanner input = new Scanner(System.in);

        while(bool){
            printMenu();
            int choice = input.nextInt();
            switch(choice){
                case 1:
                    bank.addAccount();
                    break;
                case 2:
                    bank.addCreditAccount();
                    break;
                case 3:
                    bank.addMoney(); //tehtävässä tulostukset samat, joten turha tehdä erillistä metodia
                    break;
                case 4:
                    bank.takeMoney(); //tehtävässä tulostukset samat, joten turha tehdä erillistä metodia
                    break;
                case 5:
                    bank.deleteAccount();
                    break;
                case 6:
                    bank.printAccount();
                    break;
                case 7:
                    bank.printAll();
                    break;
                case 0:
                    bool = false;
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
            }
        }
    }
    public static void printMenu(){
        System.out.println("\n*** PANKKIJÄRJESTELMÄ ***"
                + "\n1) Lisää tavallinen tili"
                + "\n2) Lisää luotollinen tili"
                + "\n3) Tallenna tilille rahaa"
                + "\n4) Nosta tililtä"
                + "\n5) Poista tili"
                + "\n6) Tulosta tili"
                + "\n7) Tulosta kaikki tilit"
                + "\n0) Lopeta");
        System.out.print("Valintasi: ");
    }
}
