/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mainBank2;

/**
 *
 * @author Konsta
 */
public abstract class Account{
    protected double balance;
    protected String accountNumber;
    protected boolean accountType;
    protected double creditLimit;


    /*public Account(double startbalance, String newAccountNumber, boolean newAccountType){
        balance = startbalance;
        accountNumber = newAccountNumber;
        accountType = newAccountType;
    }*/
        //metodit
    public void setCreditLimit(double newCreditLimit){
        creditLimit = newCreditLimit;
    }
    public double getCreditLimit(){
        return creditLimit;
    }
    public String getAccountNumber(){
        return accountNumber;
    }
    public void setBalance(double amount){
        balance += amount;
    }
    public double getBalance(){
        return balance;
    }
    public void setAccountType(boolean bool){
        accountType = bool;
    }
    public boolean getAccountType(){
        return accountType;
    }

}
class CurrentAccount extends Account{
    public CurrentAccount(double startbalance, String newAccountNumber){
        balance = startbalance;
        accountNumber = newAccountNumber;
        accountType = false;            
    }
}
class CreditAccount extends Account{      
    public CreditAccount(double startbalance, String newAccountNumber, double newCreditLimit){
        balance = startbalance;
        accountNumber = newAccountNumber;
        accountType = true;
        creditLimit = newCreditLimit;
    }

}

