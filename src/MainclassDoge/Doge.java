/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MainclassDoge;

/**
 *
 * @author Konsta
 */
import java.util.Scanner;
public class Doge {
    private String name;
    private String says;
    private int i;
    
    Doge(String input1, String input2){
        name = input1;
        says = input2;
    }
    
    public void speak(String input3){
        Scanner scan = new Scanner(System.in);
        String trimmedsays = input3.trim();
        if(trimmedsays.isEmpty()){
            while(trimmedsays.isEmpty()){
                says = "Much Wow!";
                System.out.println(name +": "+ says);
                System.out.print("Mitä koira sanoo: ");
                says = scan.nextLine();
                trimmedsays = says.trim();
            }
        }
        else{
            String[] temp = says.split(" ");
            for(i = 0;i < temp.length ; i++){
                System.out.println(temp[i]);
            }
        }
        System.out.println(name +  ": " + says);
    }
    
}
