/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package webbrowser;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

/**
 *
 * @author Taisto
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private WebView webView;
    @FXML
    private Button forwardButton;
    @FXML
    private Button backButton;
    @FXML
    private Button refreshButton;
    @FXML
    private TextField urlInput;
    @FXML
    private MenuButton menuButton;
    @FXML
    private MenuItem shoutOutButton;
    @FXML
    private MenuItem initilizeButton;
    @FXML
    private Button loadButton;
    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void nextPage(ActionEvent event) {
        if(Cache.getInstance().getLitr().hasNext()){
            Object text = Cache.getInstance().getLitr().next();
            urlInput.setText(String.valueOf(text));
            webView.getEngine().load(urlInput.getText());
        }
    }

    @FXML
    private void previousPage(ActionEvent event) {
        if(Cache.getInstance().getLitr().hasPrevious()){
            Object text = Cache.getInstance().getLitr().previous();
            urlInput.setText(String.valueOf(text));
            webView.getEngine().load(urlInput.getText());
        }
    }

    @FXML
    private void refreshPage(ActionEvent event) {
        webView.getEngine().load(urlInput.getText());
        Cache.getInstance().getArrList().add(urlInput.getText());
        Cache.getInstance().setLitr();
    }

    @FXML
    private void shoutOut(ActionEvent event) {
        webView.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void initialize(ActionEvent event) {
        webView.getEngine().executeScript("initialize()");
    }

    @FXML
    private void loadIndex(ActionEvent event) {
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
    }
    
}
