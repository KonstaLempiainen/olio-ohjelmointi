/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package webbrowser;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author Konsta
 */
public class Cache {
    private ArrayList<String> arrList;
    private ListIterator litr;

    public void setLitr() {
        this.litr = arrList.listIterator(arrList.size());
    }
    private static Cache cache = null;

    private Cache(){
        arrList  = new ArrayList();
    }
    public static Cache getInstance(){
        if(cache == null){
            cache = new Cache();
        }
        return cache;
    }
    public ArrayList<String> getArrList() {
        return arrList;
    }

    public ListIterator getLitr() {
        return litr;
    }
    
}
