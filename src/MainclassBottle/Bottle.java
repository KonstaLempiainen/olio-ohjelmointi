package MainclassBottle;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Konsta
 */
public class Bottle {
    private String type;
    private String brand;
    private String size;
    private double price;
    Bottle(){
        type = "Pepsi Max";
        brand = "Pepsi";
        size = "0.5";
        price = 1.8;
    }
    public String getBottleType(){
        return type;
    }
    public String getBottleSize(){
        return size;
    }
    public String getBottleBrand(){
        return brand;
    }
	public double getBottlePrice(){
        return price;
    }
    public void setBottleType(String type){
        this.type = type;
    }
    public void setBottleSize(String size){
        this.size = size;
    }
    public void setBottleBrand(String brand){
        this.brand = brand;
    }
	public void setBottlePrice(double price){
        this.price = price;
    }
}