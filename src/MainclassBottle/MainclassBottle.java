/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainclassBottle;


import java.util.Scanner;

/**
 *
 * @author Konsta
 */
public class MainclassBottle {

static int totalMoney;

    public static void main(String[] args) {
        BottleDispenser machine = new BottleDispenser();
        boolean start = true;
        while(start){
            System.out.print("*** LIMSA-AUTOMAATTI ***\n"
                    + "1) Lisää rahaa koneeseen\n"
                    + "2) Osta pullo\n"
                    + "3) Ota rahat ulos\n"
                    + "4) Listaa koneessa olevat pullot\n"
                    + "0) Lopeta\n");
            Scanner input = new Scanner(System.in);
            System.out.print("Valintasi: ");
            String choise = input.nextLine();
            switch(choise){
                case "1":
                    machine.money = machine.putMoney(1, machine.money);
                    break;
                case "2":
                    machine.money = machine.getBottle(machine.money);
                    break;
                case "3":
                    System.out.println(machine.money + " takaisin.");
                    break;
                case "4":
                    for(int i = 0;i < machine.arrlist.size(); i++){
                        Bottle temp = machine.arrlist.get(i);
                        System.out.println(i + ". Nimi: " + temp.getBottleType());
                        System.out.println("\t" +temp.getBottleSize()+"\t" + temp.getBottlePrice());
                    }
                    break;
                default: 
                    start = false;
                    break;
            }
        }
    }
    
}
