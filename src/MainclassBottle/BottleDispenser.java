package MainclassBottle;

import java.util.ArrayList;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Konsta
 */
public class BottleDispenser {
    double money;
    int bottlesRemaining;
    Bottle pepsi = new Bottle();
    ArrayList<Bottle> arrlist = new ArrayList<>(6);


    
    BottleDispenser(){
        money = 0;
        bottlesRemaining = 5;
        arrlist.add(pepsi);
        arrlist.add(pepsi);
        arrlist.add(pepsi);
        arrlist.add(pepsi);
        arrlist.add(pepsi);
        arrlist.add(pepsi);
    }
    
    public double getBottle(double money){
        if(money < pepsi.getBottlePrice()){
            System.out.println("Syötä rahaa ensin!");
            return money;
        }
        else if(bottlesRemaining == 0){
            System.out.println(pepsi.getBottleType()+" on loppu!");
            return money;
        }
        else{
            System.out.println("KACHUNK!"+ pepsi.getBottleType() +" tipahti masiinasta!");
            money -= pepsi.getBottlePrice();
            return money;
        }
    }
    public double putMoney(double amount, double money){
        System.out.println("Klink! Lisää rahaa laitteeseen!");
        money += amount;
        return money;
    }
    
}
