/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package texteditor;
import java.io.*;
/**
 *
 * @author Konsta
 */
public class WriteIO {
    
    public void write(String output, String text){
        try{
            BufferedWriter out = new BufferedWriter(new FileWriter(output, true));
            out.write(text);
            out.newLine();
            out.close();
        }
        catch(IOException i){
            System.out.println("Tiedostoa ei löydy.");
        }
    }
}
