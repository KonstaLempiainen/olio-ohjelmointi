/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package texteditor;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Taisto
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button load;
    @FXML
    private TextArea textArea;
    @FXML
    private Button save;
    @FXML
    private TextField inputSave;
    @FXML
    private TextField inputLoad;
    @FXML
    private Label label1;
    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void loadText(ActionEvent event) {
        String file = inputLoad.getText();
        ReadIO reader = new ReadIO();
        textArea.setText(reader.read(file));
        inputLoad.setText("");
    }

    @FXML
    private void saveText(ActionEvent event) {
        String file = inputSave.getText();
        String text = textArea.getText();
        WriteIO writer = new WriteIO();
        writer.write(file, text);
        inputSave.setText("");
    }
    
}
