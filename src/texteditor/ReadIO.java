/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package texteditor;
import java.io.*;
import java.util.*;
/**
 *
 * @author Konsta
 */
public class ReadIO {

    public String read(String fileName){
        String text = "";
        try{
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String inputLine;
            while((inputLine = in.readLine()) != null){//jotta lukee kaikki rivit eikä vain ensimmäistä.
                text = text + "\n" + inputLine;
        }
        }catch(IOException i){
            System.out.println("Tiedostoa ei löydy");
        }
        return text;
    }
}
