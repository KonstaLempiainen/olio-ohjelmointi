/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package templatejavafx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;

/**
 * FXML Controller class
 *
 * @author Taisto
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button addMoneyButton;
    @FXML
    private Label label;
    @FXML
    private MenuButton buyMenuButton;
    @FXML
    private Button returnMoneyButton;
    @FXML
    private Label message;
    @FXML
    private Button receptButton;
    @FXML
    private Label moneyAmount;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void addMoney(ActionEvent event) {
    }

    @FXML
    private void selectBuyBottle(ActionEvent event) {
    }

    @FXML
    private void returnMoney(ActionEvent event) {
    }
    
}
