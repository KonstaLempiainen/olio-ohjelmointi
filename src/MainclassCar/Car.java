/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package MainclassCar;

/**
 *
 * @author Konsta
 */
public class Car {
    public class Chassis {
        public void makePart(){
            System.out.println("Valmistetaan: Chassis");
        }
    }
    public class Body {
        public void makePart(){
            System.out.println("Valmistetaan: Body");
        }
    }
    public class Wheel {
        public void makePart(){
            System.out.println("Valmistetaan: Wheel");
        }
    }
    public class Engine {
        public void makePart(){
            System.out.println("Valmistetaan: Engine");
        }
    }
    public void print(){
        System.out.println("Autoon kuuluu:");
        System.out.println("\tBody");
        System.out.println("\tChassis");
        System.out.println("\tEngine");
        System.out.println("\t4 Wheels");
    }
    Body body = new Body();
    Chassis chassis = new Chassis();
    Engine engine = new Engine();
    Wheel wheel = new Wheel();
}


