/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testmap;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Konsta
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private AnchorPane anchor;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void addPoint(MouseEvent event) {
        
        if(ShapeHandler.getShapeHandlerInstance().getPointsAr().isEmpty()){
            Point point = new Point(event);
            ShapeHandler.getShapeHandlerInstance().addPoint(point.getName(), point);
            anchor.getChildren().add(point.getCircle());
        }
        else{
            Point point = new Point(ShapeHandler.getShapeHandlerInstance().getPointsAr().get(ShapeHandler.getShapeHandlerInstance().getPointsAr().size()-1), event);
            if(checkOverlapping(point)){
                ShapeHandler.getShapeHandlerInstance().addPoint(point.getName(), point);
                anchor.getChildren().add(point.getCircle());
            }
            else{
                System.out.println("Hei, olen piste!");
            }
        }
    }
    
    private boolean checkOverlapping(Point point){
        boolean check = false;
        for(int i = 0 ; i<ShapeHandler.getShapeHandlerInstance().getPointsAr().size(); i++){
            if(ShapeHandler.getShapeHandlerInstance().getPointsAr().get(i).getCircle().intersects(point.getCircle().getBoundsInLocal())){
                check = false;
            }
            else{
                check = true;
            }
        }
        return check;
    }

    
}
