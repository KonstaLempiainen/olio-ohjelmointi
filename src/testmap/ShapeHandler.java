/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testmap;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Konsta
 */
public class ShapeHandler {
    private static ShapeHandler shapeHandler = null;
    private ArrayList<Point> pointsAr = new ArrayList();
    private HashMap<String, Point> pointsHm = new HashMap();


    private ShapeHandler(){
        
    }
    public static ShapeHandler getShapeHandlerInstance(){
        if(shapeHandler == null){
            shapeHandler = new ShapeHandler();
        }
        return shapeHandler;
    }
    public void addPoint(String name, Point point){
        pointsHm.put(name, point);
        pointsAr.add(point);
    }
    public ArrayList<Point> getPointsAr() {
        return pointsAr;
    }

    public HashMap<String, Point> getPointsHm() {
        return pointsHm;
    }
}