/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BottleDispenserFX;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;


/**
 * FXML Controller class
 *
 * @author Taisto
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button addMoneyButton;
    @FXML
    private Label label;
    @FXML
    private Button returnMoneyButton;
    @FXML
    private Label message;
    @FXML
    private Button receptButton;
    @FXML
    private Label moneyAmount;
    @FXML
    private Button buyButton;
    @FXML
    private ComboBox<BottleClass> menuButton;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Slider slider;
    @FXML
    private Label sliderValueLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        BottleDispenserClass machine = BottleDispenserClass.getInstance();
        for(int i = 0; i<machine.getArrList().size();i++){
            menuButton.getItems().add(machine.getArrList().get(i));
        }
    }    

    
    @FXML
    private void addMoney(ActionEvent event) {
        BottleDispenserClass machine = BottleDispenserClass.getInstance();
        moneyAmount.setText(Double.toString(machine.putMoney(slider.getValue())));
    }


    @FXML
    private void returnMoney(ActionEvent event) {
        BottleDispenserClass machine = BottleDispenserClass.getInstance(); 
        String moneyText = Double.toString(machine.getMoney());
        machine.setMoney(0);
        moneyAmount.setText(Double.toString(machine.getMoney()));
        message.setText("Rahaa takaisin: " + moneyText);
    }

    @FXML
    private void buySelected(ActionEvent event) {
        Receipt bill = Receipt.getInstance();
        BottleDispenserClass machine = BottleDispenserClass.getInstance();
        BottleClass bottle = menuButton.valueProperty().getValue();
        if(machine.getBottlesRemaining()>0){
            if(machine.buyBottle(bottle)){
                message.setText("KACHUNK! "+ bottle.getBottleType() +" tipahti masiinasta!");
                machine.setMoney(machine.getMoney() - bottle.getBottlePrice());
                moneyAmount.setText(Double.toString(machine.getMoney()));
                bill.listAdd(bottle);
            }
            else{
                message.setText("Syötä rahaa ensin");
            }
        }
        else{
            message.setText("Pullot on loppu");
        }
    }


    @FXML
    private void getSliderValue(MouseEvent event) {
        sliderValueLabel.setText("Syötä " + Double.toString(slider.getValue()));
    }

    @FXML
    private void printReceipt(ActionEvent event) {
        Receipt bill = Receipt.getInstance();
        bill.makeContent();
    }

}
