
package BottleDispenserFX;

import java.util.ArrayList;

/**
 *
 * @author Konsta
 */
public class BottleDispenserClass {
    private double money;
    private int bottlesRemaining;
    private ArrayList<BottleClass> arrList = new ArrayList();
    private final BottleClass pepsi = new BottleClass("Pepsi Max", "Pepsi", "0.5", 1.8);
    private final BottleClass pepsiBig = new BottleClass("Pepsi Max", "Pepsi", "1.5", 2.2);
    private final BottleClass cola = new BottleClass("Coca-Cola Zero", "Coca-Cola", "0.5", 2.0);
    private final BottleClass colaBig = new BottleClass("Coca-Cola Zero", "Coca-Cola", "1.5", 2.5);
    private final BottleClass fanta = new BottleClass("Fanta Zero", "Fanta", "0.5", 1.95);
    static private BottleDispenserClass dispenser = null;

    
    private BottleDispenserClass(){
        money = 0;
        arrList.add(pepsi);
        arrList.add(pepsiBig);
        arrList.add(cola);
        arrList.add(colaBig);
        arrList.add(fanta);
        arrList.add(fanta);
        bottlesRemaining = arrList.size();
    }
    static public BottleDispenserClass getInstance(){
        if(dispenser == null){
            dispenser = new BottleDispenserClass();
        }
        return dispenser;
    }
    
    public boolean buyBottle(BottleClass bottle){
        if(money < bottle.getBottlePrice()){
            return false;
        }
        else{
            this.bottlesRemaining --;
            return true;
        }
    }
    public double putMoney(double amount){
        this.money += amount;
        return money;
    }

    public ArrayList<BottleClass> getArrList() {
        return arrList;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getBottlesRemaining() {
        return bottlesRemaining;
    }
    
    
}