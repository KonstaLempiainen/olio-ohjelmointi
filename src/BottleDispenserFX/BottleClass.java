/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BottleDispenserFX;

/**
 *
 * @author Konsta
 */
public class BottleClass {
    private String type;
    private String brand;
    private String size;
    private double price;
    BottleClass(String type, String brand, String size, double price){
        this.type = type;
        this.brand = brand;
        this.size = size;
        this.price = price;
    }
    public String getBottleType(){
        return type;
    }
    public String getBottleSize(){
        return size;
    }
    public String getBottleBrand(){
        return brand;
    }
	public double getBottlePrice(){
        return price;
    }
    public void setBottleType(String type){
        this.type = type;
    }
    public void setBottleSize(String size){
        this.size = size;
    }
    public void setBottleBrand(String brand){
        this.brand = brand;
    }
    public void setBottlePrice(double price){
        this.price = price;
    }
    @Override
    public String toString(){
        return type +" "+size+ " "+ price+"e";
    }    
}