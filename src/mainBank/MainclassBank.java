/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainBank;
import java.util.Scanner;
/**
 *
 * @author Konsta
 */
public class MainclassBank {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean bool = true;
        Scanner input = new Scanner(System.in);

        while(bool){
            printMenu();
            int choice = input.nextInt();
            switch(choice){
                case 1:
                    addAccount();
                    break;
                case 2:
                    addCreditAccount();
                    break;
                case 3:
                    addAccount(); //tehtävässä tulostukset samat, joten turha tehdä erillistä metodia
                    break;
                case 4:
                    addAccount(); //tehtävässä tulostukset samat, joten turha tehdä erillistä metodia
                    break;
                case 5:
                    deleteAccount();
                    break;
                case 6:
                    printAccount();
                    break;
                case 7:
                    System.out.println("Tulostaa kaiken");
                    break;
                case 0:
                    bool = false;
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
            }
        }
    }
    public static void printMenu(){
        System.out.println("*** PANKKIJÄRJESTELMÄ ***"
                + "\n1) Lisää tavallinen tili"
                + "\n2) Lisää luotollinen tili"
                + "\n3) Tallenna tilille rahaa"
                + "\n4) Nosta tililtä"
                + "\n5) Poista tili"
                + "\n6) Tulosta tili"
                + "\n7) Tulosta kaikki tilit"
                + "\n0) Lopeta");
        System.out.print("Valintasi: ");
    }
    public static void addAccount(){
        Scanner input = new Scanner(System.in);
        String accountNumber;
        String amount;
        System.out.print("Syötä tilinumero: ");
        accountNumber = input.nextLine();
        System.out.print("Syötä rahamäärä: ");
        amount = input.nextLine();
        System.out.println("Tilinumero: " + accountNumber);
        System.out.println("Rahamäärä: " + amount);
    }
    public static void addCreditAccount(){
        Scanner input = new Scanner(System.in);
        String accountNumber;
        String amount;
        String credit;
        System.out.print("Syötä tilinumero: ");
        accountNumber = input.nextLine();
        System.out.print("Syötä rahamäärä: ");
        amount = input.nextLine();
        System.out.print("Syötä luotto: ");
        credit = input.nextLine();
        System.out.println("Tilinumero: " + accountNumber);
        System.out.println("Rahamäärä: " + amount);
        System.out.println("Luotto: " + credit);
    }
    public static void deleteAccount(){
        Scanner input = new Scanner(System.in);
        String accountNumber;
        System.out.print("Syötä poistettava tilinumero: ");
        accountNumber = input.nextLine();
        System.out.println("Tilinumero: " + accountNumber);
    }
        public static void printAccount(){
        Scanner input = new Scanner(System.in);
        String accountNumber;
        System.out.print("Syötä tulostettava tilinumero: ");
        accountNumber = input.nextLine();
        System.out.println("Tilinumero: " + accountNumber);
    }
}
